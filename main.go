package main

import (
	"flag"
	"fmt"
	"image/jpeg"
	"log"
	"os"
	"path/filepath"

	"github.com/nfnt/resize"
)

func main() {

	imagePath := flag.String("path", "", "Specify the jpeg image to resize or the directory of images when used with --all")
	convertAllInDirectory := flag.Bool("all", false, "Resizes all images in the current directory.")
	width := flag.Uint("width", 1024, "Width to resize images to")
	height := flag.Uint("height", 768, "Height to resize images to")
	flag.Parse()

	if *imagePath == "" {
		fmt.Println("Resizes jpeg images.")
		fmt.Println("Example: image_resizer --path ~/Pictures --all --width 1024 --height 768")
		fmt.Println("Example: image_resizer --path ~/Pictures/my_image.jpg --width 1024 --height 768")
		fmt.Println("")
		return
	}

	var resizeErr error
	if *convertAllInDirectory == true {
		resizeErr = filepath.Walk(*imagePath, func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return filepath.SkipDir
			}

			if info.IsDir() == false {
				return resizeImage(path, *width, *height)
			}

			return nil
		})

		if resizeErr != nil {
			log.Fatal(resizeErr)
		}

	} else {
		resizeErr := resizeImage(*imagePath, *width, *height)

		if resizeErr != nil {
			log.Fatal(resizeErr)
		}
	}

}

func resizeImage(imgPath string, width uint, height uint) error {
	file, err := os.Open(imgPath)
	if err != nil {
		return err
	}

	// decode jpeg into image.Image
	img, err := jpeg.Decode(file)
	if err != nil {
		return err
	}
	file.Close()

	// resize to width 1000 using Lanczos resampling
	// and preserve aspect ratio
	m := resize.Thumbnail(width, height, img, resize.Lanczos3)

	/*filename := path.Base(imgPath)
	extension := path.Ext(filename)
	name := filename[0 : len(filename)-len(extension)]
	newPath := strings.Join([]string{name, "_resized", ".jpg"}, "")
*/
	tempOutFile := fmt.Sprintf("%s.%s", imgPath, "temp")
	out, err := os.Create(tempOutFile)
	if err != nil {
		return err
	}
	defer out.Close()

	fmt.Println(imgPath)//path.Join(path.Dir(imgPath), newPath))

	// write new image to file
	err = jpeg.Encode(out, m, nil)
	if err != nil {
		return err
	}

	err = os.Remove(imgPath)
	if err != nil {
		return err
	}

	err = os.Rename(tempOutFile, imgPath)
	if err != nil {
		return err
	}

	fmt.Println(imgPath)
	return nil
}
